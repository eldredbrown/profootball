﻿using System.ComponentModel.DataAnnotations;

namespace EldredBrown.ProFootball.NETCore.Data.Entities
{
    public class League
    {
        public int ID { get; set; }

        [Required, StringLength(50)]
        public string LongName { get; set; }

        [Required, StringLength(5)]
        public string ShortName { get; set; }

        public int FirstSeasonId { get; set; }
        public int? LastSeasonId { get; set; }
    }
}
