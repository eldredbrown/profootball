﻿using EldredBrown.ProFootball.NETCore.Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace EldredBrown.ProFootball.NETCore.Data
{
    public class ProFootballDbContext : DbContext
    {
        public ProFootballDbContext(DbContextOptions<ProFootballDbContext> options)
            : base(options)
        {

        }

        public DbSet<League> Leagues { get; set; }
    }
}
