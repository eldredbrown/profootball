﻿using System.Collections.Generic;
using System.Threading.Tasks;
using EldredBrown.ProFootball.NETCore.Data.Entities;

namespace EldredBrown.ProFootball.NETCore.Data.Repositories
{
    public interface ILeagueRepository
    {
        League Add(League newLeague);
        int Commit();
        Task<int> CommitAsync();
        League Delete(int id);
        Task<IEnumerable<League>> GetLeagues();
        League GetLeague(int id);
        Task<League> GetLeagueAsync(int id);
        IEnumerable<League> GetLeaguesByLongName(string longName);
        bool LeagueExists(int id);
        League Update(League updatedLeague);
    }
}
