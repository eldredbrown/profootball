﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EldredBrown.ProFootball.NETCore.Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace EldredBrown.ProFootball.NETCore.Data.Repositories
{
    public class LeagueRepository : ILeagueRepository
    {
        private readonly ProFootballDbContext _context;

        public LeagueRepository(ProFootballDbContext context)
        {
            _context = context;
        }

        public League Add(League newLeague)
        {
            _context.Add(newLeague);

            return newLeague;
        }

        public int Commit()
        {
            return _context.SaveChanges();
        }

        public async Task<int> CommitAsync()
        {
            return await _context.SaveChangesAsync();
        }

        public League Delete(int id)
        {
            var league = GetLeague(id);

            if (league != null)
            {
                _context.Leagues.Remove(league);
            }

            return league;
        }

        public async Task<IEnumerable<League>> GetLeagues()
        {
            return await _context.Leagues.ToListAsync();
        }

        public League GetLeague(int id)
        {
            return _context.Leagues.Find(id);
        }

        public async Task<League> GetLeagueAsync(int id)
        {
            return await _context.Leagues.FindAsync(id);
        }

        public IEnumerable<League> GetLeaguesByLongName(string longName)
        {
            return _context.Leagues
                .Where(l => string.IsNullOrEmpty(longName) || l.LongName == longName)
                .OrderBy(l => l.LongName);
        }

        public bool LeagueExists(int id)
        {
            return _context.Leagues.Any(l => l.ID == id);
        }

        public League Update(League updatedLeague)
        {
            var entity = _context.Leagues.Attach(updatedLeague);
            entity.State = EntityState.Modified;

            return updatedLeague;
        }
    }
}
