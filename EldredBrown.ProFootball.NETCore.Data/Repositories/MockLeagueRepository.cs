﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EldredBrown.ProFootball.NETCore.Data.Entities;

namespace EldredBrown.ProFootball.NETCore.Data.Repositories
{
    public class MockLeagueRepository : ILeagueRepository
    {
        private IList<League> _leagues;

        public MockLeagueRepository()
        {
            _leagues = InitializeData();
        }

        public League Add(League newLeague)
        {
            _leagues.Add(newLeague);
            newLeague.ID = _leagues.Max(l => l.ID) + 1;

            return newLeague;
        }

        public int Commit()
        {
            return 0;
        }

        public Task<int> CommitAsync()
        {
            throw new NotImplementedException();
        }

        public League Delete(int id)
        {
            var league = _leagues.FirstOrDefault(l => l.ID == id);

            if (league != null)
            {
                _leagues.Remove(league);
            }

            return league;
        }

        public Task<IEnumerable<League>> GetLeagues()
        {
            throw new NotImplementedException();
        }

        public League GetLeague(int id)
        {
            return _leagues
                .SingleOrDefault(l => l.ID == id);
        }

        public Task<League> GetLeagueAsync(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<League> GetLeaguesByLongName(string longName = null)
        {
            return _leagues
                .Where(l => string.IsNullOrEmpty(longName) || l.LongName.StartsWith(longName))
                .OrderBy(l => l.LongName);
        }

        public bool LeagueExists(int id)
        {
            throw new NotImplementedException();
        }

        public League Update(League updatedLeague)
        {
            var league = _leagues.SingleOrDefault(l => l.ID == updatedLeague.ID);

            if (league != null)
            {
                league.LongName = updatedLeague.LongName;
                league.ShortName = updatedLeague.ShortName;
                league.FirstSeasonId = updatedLeague.FirstSeasonId;
                league.LastSeasonId = updatedLeague.LastSeasonId;
            }

            return league;
        }

        private static IList<League> InitializeData()
        {
            return new List<League>()
            {
                new League {
                    ID = 1,
                    LongName = "American Professional Football Association",
                    ShortName = "APFA",
                    FirstSeasonId = 1920,
                    LastSeasonId = 1921
                },
                new League {
                    ID = 2,
                    LongName = "National Football League",
                    ShortName = "NFL",
                    FirstSeasonId = 1922
                },
                new League {
                    ID = 3,
                    LongName = "All-America Football Conference",
                    ShortName = "AAFC",
                    FirstSeasonId = 1946,
                    LastSeasonId = 1949
                },
                new League {
                    ID = 4,
                    LongName = "American Football League",
                    ShortName = "AFL",
                    FirstSeasonId = 1960,
                    LastSeasonId = 1969
                },
            };
        }
    }
}
