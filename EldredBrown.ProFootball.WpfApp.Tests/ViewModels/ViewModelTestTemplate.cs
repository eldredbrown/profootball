﻿using NUnit.Framework;

namespace EldredBrown.ProFootballWPF.Tests.ViewModels
{
    [TestFixture]
    public class ViewModelTestTemplate
    {
        #region Member Fields

        #endregion Member Fields

        #region SetUp & TearDown

        [SetUp]
        public void SetUp()
        {

        }

        [TearDown]
        public void TearDown()
        {

        }

        #endregion SetUp & TearDown

        #region Test Cases

        //[TestCase]
        public void TestCase1()
        {
            // Arrange
            // TODO: Instantiate class under test.

            // TODO: Define argument variables of method under test.

            // TODO: Set up needed infrastructure of class under test.

            // Act
            // TODO: Call method under test.

            // Assert
            // TODO: Assert results of call to method under test.
        }

        #endregion Test Cases
    }
}
